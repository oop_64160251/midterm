public class App {
    public static void main(String[] args) {
        Shop boobee = new Shop("BooBee", 1000);
        Shop aker = new Shop("Aker", 99999);
        Shop minemind = new Shop("MineMind", 200);
        
        boobee.buy(5);
        boobee.print();
        aker.buy(-10);
        aker.buy(65);
        aker.print();
        minemind.buy(3);
        minemind.print();
        minemind.sell(40);
        minemind.print();
    }
}
