public class Casino {
    private String name;
    private int balance;

    public Casino(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    public boolean play(int round) {
        int num = 1000;

        int randomNum = (int) (Math.random() * 101);
        if (round < 1)
            return false;
        for (int i = 0; i < round; i++) {
            if (balance < num)
                return false;
            if (randomNum > 50) {
                balance = balance + num;
                i++;
            } else {
                balance = balance - num;
                i++;
            }
        }
        return true;
    }

    public void print() {
        System.out.println(name + " มีเงินคงเหลือ = " + balance);
    }
}