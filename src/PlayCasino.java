public class PlayCasino {
    public static void main(String[] args) {
        Casino poo = new Casino("Poo", 10000);
        Casino pee = new Casino("Pee", 10000);
        Casino pii = new Casino("Pii", 10000);

        poo.play(10);
        pee.play(100);
        pii.play(1000);
        poo.print();
        pee.print();
        pii.print();
    }
}
