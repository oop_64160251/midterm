public class Shop {
    private String name;
    private int balance;
    private int cost;

    public Shop(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    public boolean sell(int amount) {
        cost = 5;
        balance = balance + (cost * amount);
        if (amount < 1 & amount > 64)
            return false;

        return true;
    }

    public boolean buy(int amount) {
        cost = 100;
        if (amount < 1 & amount > 64)
            return false;
        if (balance < cost)
            return false;
        return true;
    }

    public void print() {
        System.out.println(name + " มีเงินคงเหลือทั้งสิ้น = " + balance);
    }
}
